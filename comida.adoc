== Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Aperitivos

* Aceitunas 
* Frutos secos
* Patatas fritas


=== Platos principales

* Chorizo picante
* Chuletas de cabeza
* Filetes de pollo
* Hamburguesas

=== Postres

* Arroz con leche
* Tarta de chocolate
* Tarta de queso
